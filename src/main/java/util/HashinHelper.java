package util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class HashinHelper {
    public static void main(String[] args) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        System.out.println("User: " + encoder.encode("user"));
        System.out.println("Admin: " + encoder.encode("admin"));
    }
}
