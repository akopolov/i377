package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Customer {

    @Id
    @SequenceGenerator(name = "my_seq", sequenceName = "customerSeq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "my_seq")
    private Long id;

    @NotNull
    @Size(min = 2 , max = 15)
    @Pattern(regexp = "^[a-zA-Z0-9-]*$")
    private String firstName;

    @NotNull
    @Size(min = 2, max = 15)
    @Pattern(regexp = "^[a-zA-Z0-9-]*$")
    private String lastName;

    @NotNull
    @Size(min = 2, max = 15)
    @Pattern(regexp = "^[a-zA-Z0-9-]*$")
    private String code;


    private String type;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id", nullable = false)
    private List<Phone> phones = new ArrayList<>();
}
