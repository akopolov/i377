package dao;

import model.Customer;

import java.util.List;

public interface ICustomerDao {
    List<Customer> getAll();
    Customer getById(Long id);
    void add(Customer person);
    void deleteById(Long id);
    void deleteAll();
    List findAllWithKey(String key);
}
