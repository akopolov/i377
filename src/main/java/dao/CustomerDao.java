package dao;

import model.Customer;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerDao implements ICustomerDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Customer> getAll(){
        List<Customer> customerList = new ArrayList<>();
        try{
            customerList = em.createQuery(
                    "SELECT distinct c FROM Customer c LEFT JOIN FETCH c.phones",
                    Customer.class).getResultList();
        }catch (NoResultException ex){}
        return customerList;
    }

    @Override
    public Customer getById(Long id){
        Customer customer = new Customer();
        try{
            customer = em.createQuery(
                    "SELECT c FROM Customer c LEFT JOIN FETCH c.phones WHERE c.id = :id ",
                    Customer.class)
                    .setParameter("id", id)
                    .getSingleResult();
        }catch (NoResultException ex){ }

        return customer;
    }

    @Override
    public List findAllWithKey(String key) {
        String searchKey = "%" + key + "%";
        List<Customer> customerList = new ArrayList<>();
        try{
            customerList = em.createQuery(
                    "SELECT distinct c FROM Customer c LEFT JOIN fetch c.phones WHERE " +
                            "UPPER(c.firstName) LIKE UPPER(:searchKey) OR " +
                            "UPPER(c.lastName) LIKE UPPER(:searchKey) OR " +
                            "UPPER(c.code) LIKE UPPER(:searchKey)")
                    .setParameter("searchKey", searchKey)
                    .getResultList();
        }catch (NoResultException ex){}
        return customerList;
    }

    @Override
    @Transactional
    public void add(Customer customer){
        if(customer.getId() == null){
            em.persist(customer);
        }else {
            em.merge(customer);
        }
    }

    @Override
    @Transactional
    public void deleteById(Long id){
        em.createQuery("DELETE FROM Customer c WHERE c.id =:id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    @Transactional
    public void deleteAll(){
        em.createQuery("DELETE FROM Customer").executeUpdate();
    }
}
