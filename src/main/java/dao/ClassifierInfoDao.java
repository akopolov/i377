package dao;

import org.springframework.stereotype.Repository;
import util.JpaUtil;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Arrays;
import java.util.List;

@Repository
public class ClassifierInfoDao implements IClassifierInfoDao {

    @Override
    public List<String> getCustomerTypes() {
        return Arrays.asList("customer_type.private","customer_type.corporate");
    }

    @Override
    public List<String> getPhoneTypes() {
        return Arrays.asList("phone_type.fixed","phone_type.mobile");
    }
}
