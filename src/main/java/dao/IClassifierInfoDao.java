package dao;

import java.util.List;

public interface IClassifierInfoDao {
    List<String> getCustomerTypes();
    List<String> getPhoneTypes();
}
