package controller;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

@RestController
public class UserController {

    @GetMapping("users")
    public void getAllUsers(HttpServletResponse response,
                            HttpServletRequest request){
        if(request.isUserInRole("ROLE_ADMIN")){
            response.setStatus(HttpServletResponse.SC_OK);
        }else{
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

    @GetMapping("users/{userName}")
    public String getUserByName(@PathVariable String userName,
                                Principal principal,
                                HttpServletRequest request,
                                HttpServletResponse response,
                                Authentication authentication){

        if(userName.equals(principal.getName()) || request.isUserInRole("ROLE_ADMIN")){
            response.setStatus(HttpServletResponse.SC_OK);
        }else{
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }

        return principal.toString();
    }
}
