package controller;

import dao.IClassifierInfoDao;
import model.ClassifierInfo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class ClassifierController {

    @Resource
    private IClassifierInfoDao classifierInfoDao;

    @GetMapping("classifiers")
    public ClassifierInfo getAllClassifiers(){
        return new ClassifierInfo(
                classifierInfoDao.getCustomerTypes(),
                classifierInfoDao.getPhoneTypes()
        );
    }
}
