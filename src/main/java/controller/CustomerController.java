package controller;

import dao.ICustomerDao;
import model.Customer;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import validation.ValidationErrors;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@RestController
public class CustomerController {

    @Resource
    private ICustomerDao customerDao;

    @GetMapping("customers")
    public List<Customer> getAllCustomers(){
        return customerDao.getAll();
    }

    @GetMapping("customers/{id}")
    public Customer getCustomerById(@PathVariable Long id){
        return customerDao.getById(id);
    }

    @GetMapping("customers/search")
    public List<Customer> getSearchCustomers(@RequestParam(value = "key",required = false) String key){
        return customerDao.findAllWithKey(key);
    }

    @PostMapping("customers")
    public void postCustomer(@RequestBody @Valid Customer customer){
        customerDao.add(customer);
    }

    @DeleteMapping("customers")
    public void deleteAllCustomers(){
        customerDao.deleteAll();
    }

    @DeleteMapping("customers/{id}")
    public void deleteCustomer(@PathVariable Long id){
        customerDao.deleteById(id);
    }

    public ValidationErrors handleMethodArgumentNotValid(
            MethodArgumentNotValidException exception) {
        return null;
    }
}
