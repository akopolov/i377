package conf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import security.RestAuthenticationFilter;
import security.handlers.RestAuthFailureHandler;
import security.handlers.RestAuthSuccessHandler;
import security.handlers.RestLogoutSuccessHandler;

import javax.servlet.Filter;
import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    @Override
    protected void configure(HttpSecurity http)
            throws Exception {

        http.csrf().disable();

        http.authorizeRequests()
                .antMatchers("/api/login").permitAll()
                .antMatchers("/api/classifiers").permitAll()
                .antMatchers("/api/**").hasAnyRole("USER","ADMIN");

        http.formLogin()
                .loginPage("/api/login")
                .permitAll();

        http.logout()
                .logoutUrl("/api/logout")
                .logoutSuccessHandler(new RestLogoutSuccessHandler());

        http.addFilterAfter(restLoginFilter("/api/login"), LogoutFilter.class);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder builder)
            throws Exception {

        builder.jdbcAuthentication()
                .dataSource(dataSource)
                .passwordEncoder(new BCryptPasswordEncoder());
    }

    public Filter restLoginFilter(String url)
            throws Exception {

        RestAuthenticationFilter filter = new RestAuthenticationFilter(url);
        filter.setAuthenticationManager(authenticationManager());
        filter.setAuthenticationSuccessHandler(new RestAuthSuccessHandler());
        filter.setAuthenticationFailureHandler(new RestAuthFailureHandler());
        return filter;
    }
}