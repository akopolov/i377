DROP SCHEMA public CASCADE;

CREATE SEQUENCE customerSeq AS INTEGER START WITH 1;
CREATE SEQUENCE phoneSeq AS INTEGER START WITH 1;

CREATE TABLE Customer (
  id BIGINT NOT NULL PRIMARY KEY,
  firstName VARCHAR (255),
  lastName VARCHAR (255),
  code VARCHAR (100),
  type VARCHAR (100)
);

CREATE TABLE Phone (
  id BIGINT NOT NULL PRIMARY KEY,
  customer_id BIGINT NOT NULL,
  type VARCHAR (255),
  value VARCHAR (255),
);

CREATE TABLE USERS (
  username VARCHAR (255) NOT NULL PRIMARY KEY,
  password VARCHAR (255) NOT NULL,
  enabled BOOLEAN NOT NULL,
  first_name VARCHAR (255) NOT NULL
);

CREATE TABLE AUTHORITIES (
  username VARCHAR (50) NOT NULL,
  authority VARCHAR (50) NOT NULL,
  FOREIGN KEY (username) REFERENCES USERS
    ON DELETE CASCADE
);

CREATE UNIQUE INDEX ix_auth_username ON AUTHORITIES (username, authority);

INSERT INTO Customer (id, firstName, lastName, code, type)
  VALUES (NEXT VALUE FOR customerSeq, 'asd', 'asd', 'asd', 'customer_type.private');

INSERT INTO Phone (id, customer_id, type, value)
  VALUES (NEXT VALUE FOR phoneSeq, 1, 'phone_type.mobile', '123');

INSERT INTO Customer (id, firstName, lastName, code, type)
  VALUES (NEXT VALUE FOR customerSeq, 'qwe', 'qwe', 'qwe', 'customer_type.corporate');

INSERT INTO Phone (id, customer_id, type, value)
  VALUES (NEXT VALUE FOR phoneSeq, 1, 'phone_type.fixed', '321');

INSERT INTO USERS (username, password, enabled, first_name)
  VALUES ('user', '$2a$10$WecTwyF9119eO1iGT/M5Nul6EpzNuLCo6QbYEEzUlmEtQ2davuZry', TRUE, 'Jack');

INSERT INTO USERS (username, password, enabled, first_name)
  VALUES ('admin', '$2a$10$ahpnbbBHRB3G0MYlKwFVkOzfSanr9yeNOZTg4gXzscEsRnosGgLny', TRUE, 'Jill');

INSERT INTO AUTHORITIES (username, authority)
  VALUES ('user', 'ROLE_USER');

INSERT INTO AUTHORITIES (username, authority)
  VALUES ('admin', 'ROLE_ADMIN');